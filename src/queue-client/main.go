package main

import (
	"bufio"
	"dsq"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

var (
	FileName  string
	FlowSpeed int64
	Scale     int
)

const (
	FilePathArgument  = "file_path"
	FlowSpeedArgument = "flow_speed"
	ScaleArgument     = "scale"
)

func init() {
	flag.StringVar(&FileName, FilePathArgument, "fibonacci.storage", "File to store values from queue")
	flag.Int64Var(&FlowSpeed, FlowSpeedArgument, dsq.MaxSpeed, "Flow speed")
	flag.IntVar(&Scale, ScaleArgument, 1, "Flow speed")
	flag.Parse()

	dsq.Assert(FileName != "", fmt.Sprintf(`Argument "%s" required`, FilePathArgument))
	dsq.Assert(FlowSpeed > 0, fmt.Sprintf(`Argument "%s" must be positive`, FlowSpeedArgument))
	dsq.Assert(Scale > 0, fmt.Sprintf(`Argument "%s" must be positive`, ScaleArgument))

	os.Create(FileName)
}

func main() {
	connection, err := net.Dial("unix", "/tmp/echo.sock")
	if err != nil {
		log.Fatal(err)
	}
	defer connection.Close()

	command := dsq.NewFileWriterCommand(FileName)

	messageQueue := make(chan int64)
	statusQueue := make(chan bool)

	consumer := dsq.NewConsumerCluster(messageQueue, statusQueue, dsq.Speed(FlowSpeed), &command, Scale)

	go consumer.Run()

	for {
		message, err := bufio.NewReader(connection).ReadString('\n')
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Print("Message: " + message)

		value, err := strconv.ParseInt(strings.TrimSuffix(message, "\n"), 10, 64)
		if err != nil {
			log.Fatal(err)
		}

		messageQueue <- value

		fmt.Fprintf(connection, "Success\n")

		<-statusQueue
	}
}
