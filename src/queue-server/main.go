package main

import (
	"bufio"
	"dsq"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"time"
)

var (
	GenerationSpeed int64
)

const (
	GenerationSpeedArgument = "generation_speed"
)

func init() {
	flag.Int64Var(&GenerationSpeed, GenerationSpeedArgument, dsq.MaxSpeed, "Value generation speed")
	flag.Parse()

	dsq.Assert(GenerationSpeed > 0, fmt.Sprintf(`Argument "%s" must be positive`, GenerationSpeedArgument))
}

func main() {
	dsq.DebugInfo("Server running!")
	listener, err := net.Listen("unix", "/tmp/echo.sock")

	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt)
	go func(listener net.Listener, c <-chan os.Signal) {
		for sig := range c {
			log.Printf("Caught signal %s: shutting down.", sig)
			listener.Close()
			os.Exit(0)
		}
	}(listener, signalChannel)

	if err != nil {
		dsq.DebugError(err)

		return
	}

	connection, err := listener.Accept()

	if err != nil {
		dsq.DebugError(err)

		return
	}

	generator := dsq.NewFibonacciGenerator(1, 1)

	messageQueue := make(chan int64)
	statusQueue := make(chan bool)

	generatorTicker := time.NewTicker(dsq.Speed(GenerationSpeed))

	producer := dsq.NewProducer(messageQueue, statusQueue, generatorTicker, &generator)

	go producer.Run()

	for {
		value := <-messageQueue
		connection.Write([]byte(strconv.FormatInt(value, 10) + "\n"))

		message, err := bufio.NewReader(connection).ReadString('\n')

		if err != nil {
			dsq.DebugError(err)
			listener.Close()

			return
		}

		dsq.DebugInfo(message)

		statusQueue <- true
	}
}
