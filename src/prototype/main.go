package main

import (
	"dsq"
	"flag"
	"fmt"
	"os"
	"time"
)

var (
	FileName        string
	GenerationSpeed int64
	FlowSpeed       int64
	Scale           int
)

const (
	FilePathArgument        = "file_path"
	GenerationSpeedArgument = "generation_speed"
	FlowSpeedArgument       = "flow_speed"
	ScaleArgument           = "scale"
)

func init() {
	flag.StringVar(&FileName, FilePathArgument, "fibonacci.storage", "File to store values from queue")
	flag.Int64Var(&GenerationSpeed, GenerationSpeedArgument, dsq.MaxSpeed, "Value generation speed")
	flag.Int64Var(&FlowSpeed, FlowSpeedArgument, dsq.MaxSpeed, "Flow speed")
	flag.IntVar(&Scale, ScaleArgument, 1, "Flow speed")
	flag.Parse()

	dsq.Assert(FileName != "", fmt.Sprintf(`Argument "%s" required`, FilePathArgument))
	dsq.Assert(GenerationSpeed > 0, fmt.Sprintf(`Argument "%s" must be positive`, GenerationSpeedArgument))
	dsq.Assert(FlowSpeed > 0, fmt.Sprintf(`Argument "%s" must be positive`, FlowSpeedArgument))
	dsq.Assert(Scale > 0, fmt.Sprintf(`Argument "%s" must be positive`, ScaleArgument))

	os.Create(FileName)
}

func main() {
	generator := dsq.NewFibonacciGenerator(7, 8)
	command := dsq.NewFileWriterCommand(FileName)

	messageQueue := make(chan int64)
	statusQueue := make(chan bool)

	generatorTicker := time.NewTicker(dsq.Speed(GenerationSpeed))

	producer := dsq.NewProducer(messageQueue, statusQueue, generatorTicker, &generator)
	consumer := dsq.NewConsumerCluster(messageQueue, statusQueue, dsq.Speed(FlowSpeed), &command, Scale)

	go producer.Run()
	go consumer.Run()
	time.Sleep(time.Second)
	producer.Stop()
	time.Sleep(time.Second)
	consumer.Stop()
}
