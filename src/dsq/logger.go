package dsq

import "fmt"

func TI() string {
	return "\x1b[92m[INFO]\x1b[0m"
}

func TE() string {
	return "\x1b[91m[ERROR]\x1b[0m"
}

func DebugInfo(a ...interface{}) {
	fmt.Println(TI(), a)
}

func DebugError(a ...interface{}) {
	fmt.Println(TE(), a)
}
