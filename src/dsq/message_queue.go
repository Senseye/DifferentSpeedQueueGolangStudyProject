package dsq

import (
	"time"
)

type (
	Process interface {
		Run()
		Stop()
	}

	RunStatus struct {
		running bool
	}

	Generator interface {
		Generate() int64
	}

	Command interface {
		Execute(int64)
	}

	Producer struct {
		RunStatus
		messageQueue chan<- int64
		statusQueue  <-chan bool
		ticker       *time.Ticker
		generator    Generator
	}

	Consumer struct {
		RunStatus
		messageQueue <-chan int64
		statusQueue  chan<- bool
		ticker       *time.Ticker
		command      Command
	}

	ProcessCluster struct {
		list []Process
	}
)

func NewProducer(messageQueue chan<- int64, statusQueue <-chan bool, ticker *time.Ticker, generator Generator) Process {
	return &Producer{RunStatus{false}, messageQueue, statusQueue, ticker, generator}
}

func NewConsumer(queue <-chan int64, statusQueue chan<- bool, ticker *time.Ticker, command Command) Process {
	return &Consumer{RunStatus{false}, queue, statusQueue, ticker, command}
}

func NewConsumerCluster(queue <-chan int64, statusQueue chan<- bool, period time.Duration, command Command, size int) Process {
	list := make([]Process, 0, size)
	for i := 0; i < size; i++ {
		list = append(list, NewConsumer(queue, statusQueue, time.NewTicker(period), command))
	}
	return &ProcessCluster{list}
}

func (rs *RunStatus) Stop() {
	rs.running = false
}

func (p *Producer) Run() {
	p.running = true
	for p.running {
		select {
		case <-p.ticker.C:
			p.messageQueue <- p.generator.Generate()

			<-p.statusQueue
		}
	}
}

func (c *Consumer) Run() {
	c.running = true
	for c.running {
		select {
		case <-c.ticker.C:
			c.command.Execute(<-c.messageQueue)

			c.statusQueue <- true
		}
	}
}

func (c *ProcessCluster) Run() {
	for i := range c.list {
		go c.list[i].Run()
	}
}

func (c *ProcessCluster) Stop() {
	for i := range c.list {
		go c.list[i].Stop()
	}
}
