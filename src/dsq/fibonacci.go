package dsq

// https://en.wikipedia.org/wiki/Fibonacci

type (
	FibonacciGenerator struct {
		a int64
		b int64
	}
)

func NewFibonacciGenerator(a, b int64) FibonacciGenerator {
	return FibonacciGenerator{a, b}
}

func (g *FibonacciGenerator) Generate() int64 {
	next := g.a + g.b

	g.a, g.b = g.b, next

	return next
}
