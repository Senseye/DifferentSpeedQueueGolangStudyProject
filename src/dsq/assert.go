package dsq

import "log"

func Assert(success bool, message string) {
	if success == false {
		log.Fatal(message)
	}
}
