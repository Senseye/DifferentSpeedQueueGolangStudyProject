package dsq

import "time"

const (
	MaxSpeed = 1e9
)

func Speed(times int64) time.Duration {
	if times > MaxSpeed {
		return time.Nanosecond
	}

	if times > 1 {
		return time.Duration(MaxSpeed/times) * time.Nanosecond
	}

	return time.Second
}
