package dsq

import "testing"

func TestFibonacciGenerator_Generate(t *testing.T) {
	generator := NewFibonacciGenerator(1, 1)

	assertSame(t, 2, generator.Generate())
	assertSame(t, 3, generator.Generate())
	assertSame(t, 5, generator.Generate())
	assertSame(t, 8, generator.Generate())
	assertSame(t, 13, generator.Generate())
}

func assertSame(t *testing.T, expected, actual int64) {
	if expected != actual {
		t.Error("Expect:", expected, "but got:", actual)
	}
}
