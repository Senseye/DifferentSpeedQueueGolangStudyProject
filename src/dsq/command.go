package dsq

import (
	"log"
	"os"
	"strconv"
)

type (
	FileWriterCommand struct {
		fileName string
	}
)

func NewFileWriterCommand(fileName string) FileWriterCommand {
	return FileWriterCommand{fileName}
}

func (c *FileWriterCommand) Execute(value int64) {
	f, err := os.OpenFile(c.fileName, os.O_APPEND|os.O_WRONLY, 0600)

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	if _, err := f.WriteString(strconv.FormatInt(value, 10) + "\n"); err != nil {
		log.Fatal(err)
	}
}
