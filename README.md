# Exists some components with differents speed of write & read message

### Testing
```bash
export GOPATH=$(pwd)
```

##### Prototype:
```bash
go run ./src/prototype/main.go -generation_speed=1000000000 -flow_speed=3 -scale=2 -file_path=fibonacci.storage && cat fibonacci.storage
```

##### Server & Client
```bash
go run ./src/queue-server/main.go -generation_speed=1000000000
```

```bash
go run ./src/queue-client/main.go -flow_speed=1 -scale=2 -file_path=fibonacci.storage
tail fibonacci.storage -f
```